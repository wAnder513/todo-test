import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'todo',
      component: () => import('../views/todo-create/TodoCreate.vue')
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: () => import('../views/edit-task/EditTasks.vue')
    }
  ]
})

export default router
