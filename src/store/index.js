import { createStore } from 'vuex'

export default createStore({
  state: {
    tasks: [],
    currentTask: {}
  },
  getters: {
    getTasks(state) {
      return state.tasks
    },
    getCurrentTask(state) {
      return state.currentTask
    }
  },
  mutations: {
    SET_TASK(state, task) {
      state.tasks.unshift(task)
    },
    SET_REMOVE_TASK(state, id) {
      state.tasks = [...state.tasks].filter((task) => task.id !== id)
    },
    SET_TASK_STATUS_CHANGE(state, id) {
      state.tasks.forEach((task) => {
        if(task.id === id) {
          task.status = 'completed'
        }
      }) 
    },
    SET_CURRENT_TASK(state, id) {
      state.tasks.forEach((task) => {
        if(task.id === id) {
          state.currentTask = task
        }
      })
    },
    SET_EDIT_TASK(state, edit) {
      state.tasks.forEach((task) => {
        if(task.id === edit.id) {
          task.text = edit.text
        }
      })
    }
  },
  actions: {
    addTask({commit}, task) {
      commit('SET_TASK', task)
    },
    removeTask({commit}, id) {
      commit('SET_REMOVE_TASK', id)
    },
    changeTaskStatus({commit}, id) {
      commit('SET_TASK_STATUS_CHANGE', id)
    },
    addCurrentTask({commit}, id) {
      commit('SET_CURRENT_TASK', id)
    },
    editTaskText({commit}, edit) {
      commit('SET_EDIT_TASK', edit)
    }
  },
})